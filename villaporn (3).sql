-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2018 at 01:44 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `villaporn`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `pro_code` varchar(11) NOT NULL,
  `ip_add` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `cus_id` int(11) NOT NULL,
  `cus_firstname` varchar(100) NOT NULL,
  `cus_lastname` varchar(100) NOT NULL,
  `cus_username` varchar(100) NOT NULL,
  `cus_password` varchar(100) NOT NULL,
  `cus_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cus_id`, `cus_firstname`, `cus_lastname`, `cus_username`, `cus_password`, `cus_email`) VALUES
(1, 'john', 'Doe', 'john', '123456', 'aatehilla@gmail.com'),
(2, 'joe', 'john', 'bros', '123456', 'aatehilla@gmail.com'),
(3, 'james', 'john', 'tiger', '123456', 'aatehilla@gmail.com'),
(4, 'james', 'john', 'tiger', '123456', 'aatehilla@gmail.com'),
(5, 'james', 'john', 'tiger', '123456', 'aatehilla@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `ord_id` int(11) NOT NULL,
  `ord_cus_id` int(11) NOT NULL,
  `ord_pro_id` varchar(11) NOT NULL,
  `ord_qty` int(11) NOT NULL,
  `ord_date_created` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`ord_id`, `ord_cus_id`, `ord_pro_id`, `ord_qty`, `ord_date_created`) VALUES
(1, 3, 'PD1001', 2, '2017-12-23 23:31:01'),
(2, 3, 'PD1002', 3, '2017-12-23 23:31:02'),
(6, 1, 'PD1003', 2, '2018-06-17 01:14:17'),
(7, 3, 'PD1001', 1, '2018-06-17 17:23:17'),
(8, 3, 'PD1002', 1, '2018-06-17 17:23:17'),
(11, 3, 'PD1002', 10, '2018-06-18 00:16:00'),
(12, 1, 'PD1002', 3, '2018-07-03 02:48:03'),
(13, 1, 'PD1003', 2, '2018-07-03 02:48:04'),
(14, 1, '', 0, '2018-07-03 02:49:15'),
(15, 1, 'PD1003', 1, '2018-07-03 02:50:13'),
(16, 1, 'PD1004', 10, '2018-07-03 02:50:13'),
(17, 1, 'PD1004', 2, '2018-07-06 02:45:54'),
(18, 1, 'PD1003', 3, '2018-07-06 02:45:54'),
(19, 1, 'PD1004', 1, '2018-07-06 02:52:00'),
(20, 1, 'PD1004', 1, '2018-07-06 02:58:11'),
(21, 1, 'PD1004', 1, '2018-07-06 02:59:29'),
(22, 1, 'PD1004', 1, '2018-07-06 02:59:33'),
(23, 1, 'PD1004', 1, '2018-07-06 02:59:39'),
(24, 1, 'PD1004', 1, '2018-07-06 02:59:50'),
(25, 1, 'PD1004', 1, '2018-07-06 03:00:44'),
(26, 1, 'PD1004', 1, '2018-07-06 03:02:33'),
(27, 1, 'PD1004', 1, '2018-07-06 03:04:21'),
(28, 1, 'PD1004', 1, '2018-07-06 03:05:57'),
(29, 1, 'PD1004', 1, '2018-07-06 03:06:46'),
(30, 1, 'PD1004', 1, '2018-07-06 03:24:26'),
(31, 1, 'PD1004', 1, '2018-07-06 03:24:44'),
(32, 1, 'PD1004', 1, '2018-07-06 03:25:52'),
(33, 1, 'PD1004', 1, '2018-07-06 03:25:58'),
(34, 1, 'PD1004', 1, '2018-07-06 03:26:10'),
(35, 1, 'PD1004', 1, '2018-07-06 05:14:58'),
(36, 1, 'PD1004', 1, '2018-07-06 05:15:27'),
(37, 1, 'PD1004', 1, '2018-07-06 05:16:42'),
(38, 1, 'PD1004', 1, '2018-07-06 05:17:48'),
(39, 1, 'PD1004', 1, '2018-07-06 05:19:03'),
(40, 1, 'PD1004', 1, '2018-07-06 05:21:27'),
(41, 1, 'PD1004', 1, '2018-07-06 05:25:13'),
(42, 1, 'PD1004', 1, '2018-07-06 05:25:58'),
(43, 1, 'PD1004', 1, '2018-07-06 05:29:07'),
(44, 1, 'PD1004', 1, '2018-07-06 05:41:12'),
(45, 1, 'PD1004', 1, '2018-07-06 05:41:55'),
(46, 1, 'PD1004', 1, '2018-07-06 05:42:36'),
(47, 1, 'PD1004', 1, '2018-07-06 05:43:02'),
(48, 1, 'PD1004', 1, '2018-07-06 05:43:31'),
(49, 1, 'PD1004', 1, '2018-07-06 05:45:06'),
(50, 1, 'PD1004', 1, '2018-07-06 05:53:01'),
(51, 1, '', 0, '2018-07-06 05:55:48'),
(52, 1, '', 0, '2018-07-06 05:56:58'),
(53, 1, '', 0, '2018-07-06 05:57:45');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ords_id` int(11) NOT NULL,
  `ords_cus_id` int(11) NOT NULL,
  `ords_total` int(11) NOT NULL,
  `ords_invoice` int(100) NOT NULL,
  `ords_trx_id` varchar(200) NOT NULL,
  `ords_pmt_date` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'enque'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ords_id`, `ords_cus_id`, `ords_total`, `ords_invoice`, `ords_trx_id`, `ords_pmt_date`, `status`) VALUES
(1, 2, 601, 6986, '06855692427850223', '2017-12-23T22:41:50Z', 'shipped');

-- --------------------------------------------------------

--
-- Table structure for table `paylater`
--

CREATE TABLE `paylater` (
  `latr_id` int(11) NOT NULL,
  `latr_cus_id` int(11) NOT NULL,
  `latr_amt` int(11) NOT NULL,
  `latr_invoice` int(11) NOT NULL,
  `latr_date_created` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paylater`
--

INSERT INTO `paylater` (`latr_id`, `latr_cus_id`, `latr_amt`, `latr_invoice`, `latr_date_created`) VALUES
(3, 3, 2004, 6402, '2017-12-23 23:31:02'),
(4, 1, 200, 2918, '2018-06-17 01:14:17'),
(5, 3, 701, 1611, '2018-06-17 17:23:17'),
(6, 3, 5009, 9905, '2018-06-18 00:16:00'),
(7, 1, 1703, 5278, '2018-07-03 02:48:04'),
(8, 1, 0, 6518, '2018-07-03 02:49:15'),
(9, 1, 4103, 5028, '2018-07-03 02:50:14'),
(10, 1, 1101, 6161, '2018-07-06 02:45:54'),
(28, 1, 0, 9682, '2018-07-06 05:56:58'),
(29, 1, 0, 4463, '2018-07-06 05:57:45');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_code` varchar(60) NOT NULL,
  `product_name` varchar(60) NOT NULL,
  `product_desc` tinytext NOT NULL,
  `product_img_name` varchar(60) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `product_stock` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_code`, `product_name`, `product_desc`, `product_img_name`, `price`, `product_stock`) VALUES
(1, 'PD1001', 'Android Phone FX1', 'Di sertakan secara rambang yang lansung tidak munasabah. Jika anda ingin menggunakan Lorem Ipsum, anda perlu memastikan bahwa tiada apa yang', 'android-phone.jpg', '200.50', 9),
(2, 'PD1002', 'Television DXT', 'Ia menggunakan kamus yang mengandungi lebih 200 ayat Latin, bersama model dan struktur ayat Latin, untuk menghasilkan Lorem Ipsum yang munasabah.', 'lcd-tv.jpg', '500.85', 9),
(3, 'PD1003', 'External Hard Disk', 'Ada banyak versi dari mukasurat-mukasurat Lorem Ipsum yang sedia ada, tetapi kebanyakkannya telah diubahsuai, lawak jenaka diselitkan, atau ayat ayat yang', 'external-hard-disk.jpg', '100.00', 9),
(4, 'PD1004', 'Wrist Watch GE2', 'Memalukan akan terselit didalam di tengah tengah kandungan text. Semua injin Lorem Ipsum didalam Internet hanya mengulangi text, sekaligus menjadikan injin kami sebagai yang terunggul dan tepat sekali di Internet.', 'wrist-watch.jpg', '400.30', 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usr_id` int(100) NOT NULL,
  `usr_username` varchar(100) NOT NULL,
  `usr_pass` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `usr_username`, `usr_pass`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ords_id`);

--
-- Indexes for table `paylater`
--
ALTER TABLE `paylater`
  ADD PRIMARY KEY (`latr_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `cus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ords_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `paylater`
--
ALTER TABLE `paylater`
  MODIFY `latr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usr_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
