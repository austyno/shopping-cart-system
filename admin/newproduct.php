<?php 
session_start();
include_once('../functions.php');

    if(!isset($_SESSION['token'])){
        header("Location:login.php?login=required");
    }


    //generate new product code
    $pro_code =mysql_query("select product_code from products order by product_code desc limit 1");
    $pro_rs = mysql_fetch_assoc($pro_code);                    
    $str = $pro_rs['product_code'];

    $c = substr($str, 2,5);

    $cd = $c + 1;

    $procode = 'PD'.$cd;


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dash.php"><i class="fa fa-dashboard fa-fw"></i> Completed Orders</a>
                        </li>
                        
                        <li>
                            <a href="pending.php"><i class="fa fa-table fa-fw"></i>Pending Orders</a>
                        </li>
                        <li>
                            <a href="customers.php"><i class="fa fa-edit fa-fw"></i>Customers</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Products<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="products.php">Stock level</a>
                                </li>
                                <li>
                                    <a href="newproduct.php">Add new product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

    <div id="page-wrapper">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add new Product
                <a  href="products.php" style="float:right;font-size:16px" class="fa fa-angle-double-left"> Back</a>
            </div>
            <div class="panel-body">

                <form role="form" method="post" action="process.php?mode=newproduct" enctype="multipart/form-data">
                    <div class="form-group" >
                            <label for="Full name" class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-9">
                                <input style="margin-bottom:10px" type="text" class="form-control" name="pro_name" placeholder="Enter Product Name" required>
                            </div>
                    </div>

                    <div class="form-group">
                            <label for="Full name" class="col-sm-2 control-label">Product Description</label>
                            <div class="col-sm-9">
                                <textarea style="margin-bottom:10px" rows="8" class="form-control" name="pro_des" required></textarea>
                            </div>
                    </div>

                    <div class="form-group">
                            <label for="Full name" class="col-sm-2 control-label">Product Price</label>
                            <div class="col-sm-9">
                                <input style="margin-bottom:10px" type="text" class="form-control" name="pro_price" placeholder="Enter Product Price" required>
                            </div>
                    </div>

                    <div class="form-group">
                            <label for="Full name" class="col-sm-2 control-label">Product Quantity</label>
                            <div class="col-sm-9">
                                <input style="margin-bottom:10px" type="text" class="form-control" name="pro_qty" placeholder="Enter Product Quantity" required>
                            </div>
                    </div>                
                   
                    <div class="form-group">
                            <label for="Full name" class="col-sm-2 control-label">Product Image</label>
                            <div class="col-sm-9">
                                <input style="margin-bottom:10px" type="file" class="form-control" name="pro_image" placeholder="Enter Product Name" required>
                            </div>
                    </div>

                    <div class="form-group">
                            <label for="Full name" class="col-sm-2 control-label">Product Code</label>
                            <div class="col-sm-9">
                                <input style="margin-bottom:10px" type="text" class="form-control" name="code" value="<?= $procode ?>"  required readonly>
                            </div>
                    </div>

                    <div class="col-sm-9 col-sm-offset-2"><button type="submit" class="btn btn-success btn-block">Add product</button></div>
                </form>
            </div>
        </div>

    </div>
    <!-- /#wrapper -->

    </div>
    
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });



     $(document).ready(function(){
	
        $(document).on('click', '#view', function(e){
            
            e.preventDefault();
            
            var uid = $(this).data('id');   
            
            $('#dynamic-content').html(''); 
            $('#modal-loader').show();      
            
            $.ajax({
                url: 'ajax.php',
                type: 'GET',
                data: 'id='+uid+'&mode=stock',
                dataType: 'html'
            })
            .done(function(data){
                console.log(data);	
                $('#dynamic-content').html('');    
                $('#dynamic-content').html(data); 
                $('#modal-loader').hide();		  	
            })
            .fail(function(){
                $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                $('#modal-loader').hide();
            });
            
        });
    
});

    </script>




    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Order Items for </h4>
                </div>
                <div class="modal-body">
                    
                    <form method="post" action="process.php?mode=stock">
                        <label>Update level</label>
                        <input type="numbers" name="stock">
                        <input type="submit" value="update stock level">
                    </form>
                    

                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


     <div class="modal fade" id="pro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Update Stock </h4>
                </div>
                <div class="modal-body">
                    
                
                    <div id="modal-loader" style="display: none; text-align: center;">
                        <img src="ajax-loader.gif">
                    </div>        


                    <div id="dynamic-content"></div> 

                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</body>

</html>
